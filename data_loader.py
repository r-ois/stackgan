import torchvision.transforms as transforms
import torch.utils.data as data
from PIL import Image
import PIL
import os
import pickle
import numpy as np
import random

class ImTextDataset(data.Dataset):
    def __init__(self, data_dir, mode="train", transform=None):

        self.transform = transform
        
        self.data_dir = data_dir
        data_dir = os.path.join(data_dir, mode)
        self.filenames = self.load_filenames(data_dir)
        self.embeddings = self.load_embedding(data_dir)

    def load_filenames(self, data_dir):
        filepath = os.path.join(data_dir, 'filenames.pickle')
        with open(filepath, 'rb') as f:
            filenames = pickle.load(f)
        print('Load filenames from: %s (%d)' % (filepath, len(filenames)))
        return filenames

    def load_embedding(self, data_dir):
        embedding_filename = os.path.join(data_dir, 'char-CNN-RNN-embeddings.pickle')
        #print(data_dir + embedding_filename)
        with open(embedding_filename, 'rb') as f:
            embeddings = pickle.load(f, encoding='latin1')
            embeddings = np.array(embeddings)
            print('embeddings: ', embeddings.shape)
        return embeddings

    def __getitem__(self, index):
        key = self.filenames[index]
        embeddings = self.embeddings[index,:,:]

        embedding_ix = random.randint(0, embeddings.shape[0]-1)
        embedding = embeddings[embedding_ix, :]
        img_name = '%s/images/%s.jpg' % (self.data_dir, key)
        img = self.get_img(img_name)
        
        return img, embedding

    def __len__(self):
        return len(self.filenames)
        
    def get_img(self, img_name):
        img = Image.open(img_name).convert('RGB')
        width, height = img.size
        load_size = int(256 * 76 / 64)
        img = img.resize((load_size, load_size), PIL.Image.BILINEAR)
        
        if self.transform is not None:
            img = self.transform(img)
        return img
        
        
def get_loader(data_dir, crop_size=256, image_size=256,
               batch_size=16, mode="train", num_workers=1):
    
    image_transform = transforms.Compose([
            transforms.RandomCrop(crop_size),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
    
    dataset = ImTextDataset(data_dir, mode, image_transform)
    data_loader = data.DataLoader(dataset=dataset,
                                  batch_size=batch_size,
                                  shuffle=(mode=='train'),
                                  num_workers=num_workers)
    return data_loader

