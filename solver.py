from model import Generator
from model import Discriminator
from torch.autograd import Variable
#from torchvision.utils import save_image
import torch
from torch import distributions
from torch import autograd
import torch.nn.init as init
import torch.nn.functional as F
import numpy as np
import os
import time
import datetime
import sys
import random
import math

class Solver:

    def __init__(self, loader, gpu=-1):
        
        self.loader = loader

        self.z_dim = 128
        self.image_size = 128

        self.average_beta = 0.97 #0.999

        self.g_lr = 0.0001
        self.d_lr = 0.0004
        
        self.beta1 = 0.0
        self.beta2 = 0.999
        self.model_save_step = 10000
        self.lr_update_step = 1000

        if gpu < 0:
            self.device = torch.device('cpu')
        else:
            self.device = torch.device('cuda:{}'.format(gpu) if torch.cuda.is_available() else 'cpu')

        #self.image_size = 128

        self.num_iters = 200000
        self.num_iters_decay = 0

        self.log_step = 10
        self.sample_step = 10

        # Directories.
        self.log_dir = "log"
        self.sample_dir = "sample"
        self.model_save_dir = "model"
        self.result_dir = "result"

        self.build_model()

    def build_model(self):
        
        self.G = Generator()
        self.Ds = torch.nn.ModuleList()
        self.down = torch.nn.AvgPool2d(3, 2, 1, count_include_pad=False)
        input_size = 256
        for i in range(3):
            self.Ds.append(Discriminator(input_size=input_size))
            input_size = input_size // 2
            
        print("D num:{}".format(len(self.Ds)))

        self.g_optimizer = torch.optim.Adam(self.G.parameters(), self.g_lr, [self.beta1, self.beta2])
        self.d_optimizer = torch.optim.Adam(self.Ds.parameters(), self.d_lr, [self.beta1, self.beta2])

        self.G.to(self.device)
        self.Ds.to(self.device)
        
        self.G.apply(weights_init('orthogonal'))
        self.Ds.apply(weights_init('orthogonal'))

        #update_average(self.Gt, self.G, beta=0.0)
        
    def multiD(self, x, mu):
        d_reals = []
        d_real_cs = []
        
        for model in self.Ds:
            d_real, d_real_c = model(x, mu)
            
            d_reals.append(d_real)
            d_real_cs.append(d_real_c)
            
            x = self.down(x)
            
        return d_reals, d_real_cs
        
    def train_model(self):
        self.G.train()
        #self.Gt.train()
        self.D.train()

    def eval_model(self):
        self.G.eval()
        #self.Gt.eval()
        self.D.eval()

    def restore_model(self, resume_iters):
        """Restore the trained generator and discriminator."""
        print('Loading the trained models from step {}...'.format(resume_iters))
        G_path = os.path.join(self.model_save_dir, '{}-G.ckpt'.format(resume_iters))
        #Gt_path = os.path.join(self.model_save_dir, '{}-Gt.ckpt'.format(resume_iters))
        D_path = os.path.join(self.model_save_dir, '{}-D.ckpt'.format(resume_iters))
        
        self.G.load_state_dict(torch.load(G_path, map_location=lambda storage, loc: storage))
        #self.Gt.load_state_dict(torch.load(Gt_path, map_location=lambda storage, loc: storage))
        self.D.load_state_dict(torch.load(D_path, map_location=lambda storage, loc: storage))
        
    def reset_grad(self):
        """Reset the gradient buffers."""
        self.g_optimizer.zero_grad()
        self.d_optimizer.zero_grad()

    def denorm(self, x):
        """Convert the range from [-1, 1] to [0, 1]."""
        out = (x + 1) / 2
        return out.clamp_(0, 1)

    def update_lr(self, g_lr, d_lr):
        """Decay learning rates of the generator and discriminator."""
        for param_group in self.g_optimizer.param_groups:
            param_group['lr'] = g_lr
        for param_group in self.d_optimizer.param_groups:
            param_group['lr'] = d_lr

    def label2onehot(self, labels, dim):
        """Convert label indices to one-hot vectors."""
        batch_size = labels.size(0)
        out = torch.zeros(batch_size, dim)
        out[np.arange(batch_size), labels.long()] = 1
        return out

    def gradient_penalty(self, y, x):
        """Compute gradient penalty: (L2_norm(dy/dx) - 1)**2."""
        weight = torch.ones(y.size()).to(self.device)
        dydx = torch.autograd.grad(outputs=y,
                                   inputs=x,
                                   grad_outputs=weight,
                                   retain_graph=True,
                                   create_graph=True,
                                   only_inputs=True)[0]

        dydx = dydx.view(dydx.size(0), -1)
        dydx_l2norm = torch.sqrt(torch.sum(dydx**2, dim=1))
        return torch.mean((dydx_l2norm-1)**2)

    def classification_loss(self, logit, target):
        """Compute binary or softmax cross entropy loss."""
        return F.binary_cross_entropy_with_logits(logit, target, size_average=False) / logit.size(0)

    def compute_loss(self, d_out, target):
        targets = d_out.new_full(size=d_out.size(), fill_value=target)

        loss = F.binary_cross_entropy_with_logits(d_out, targets)
        
        return loss
    
    def get_zero_tensor(self, input):
        if True: #if self.zero_tensor is None:
            self.zero_tensor = torch.FloatTensor(1).fill_(0).to(self.device)
            self.zero_tensor.requires_grad_(False)
        return self.zero_tensor.expand_as(input)
    
    
    def hinge_loss(self, inputs, target_is_real, for_discriminator=True):
        loss = 0.
        for input in inputs:
            if for_discriminator:
                if target_is_real:
                    minval = torch.min(input - 1, self.get_zero_tensor(input))
                    loss += -torch.mean(minval)
                else:
                    minval = torch.min(-input - 1, self.get_zero_tensor(input))
                    loss += -torch.mean(minval)
            else:
                assert target_is_real, "The generator's hinge loss must be aiming for real"
                loss += -torch.mean(input)
            
        return loss / len(inputs)
    
    def KL_loss(self, mu, logvar):
        # -0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
        KLD_element = mu.pow(2).add_(logvar.exp()).mul_(-1).add_(1).add_(logvar)
        KLD = torch.mean(KLD_element).mul_(-0.5)
        return KLD
    

    def train(self, start_iter=0):

        g_lr = self.g_lr
        d_lr = self.d_lr
        
        print(self.G)

        zdist = None

        if start_iter > 0:
            self.restore_model(start_iter)

        # Start training.
        print('Start training...')
        start_time = time.time()
        data_iter = iter(self.loader)
        
        batch_size = self.loader.batch_size
        
        for i in range(start_iter, self.num_iters):
            try:
                x, embedding = next(data_iter)
            except:
                
                data_iter = iter(self.loader)
                x, embedding = next(data_iter)

            # input images
            x = x.to(self.device)#.requires_grad_()
            embedding = embedding.to(self.device)
            if x.size(0) != batch_size:
                continue
            
            self.Ds.zero_grad()

            # train discriminator
            #toogle_grad(self.G, False)
            #toogle_grad(self.D, True)

            z = self.G.getz(batch_size).to(self.device)
            x_fake, mu, logvar = self.G(embedding, z)
                
            #print(x.size())
            d_real, d_real_c = self.multiD(x, embedding.detach())
            d_fake, d_fake_c = self.multiD(x_fake.detach(), embedding.detach())
            
            d_real_adv = self.hinge_loss(d_real, target_is_real=True, for_discriminator=True)
            d_real_c_adv = self.hinge_loss(d_real_c, target_is_real=True, for_discriminator=True)
            d_fake_adv = self.hinge_loss(d_fake, target_is_real=False, for_discriminator=True)
            d_fake_c_adv = self.hinge_loss(d_fake_c, target_is_real=False, for_discriminator=True)
            
            dloss = 0.5 * (d_real_adv + d_fake_adv + d_real_c_adv + d_fake_c_adv)
            dloss.backward()
            self.d_optimizer.step()

            # train generator
            self.G.zero_grad()
            d_fake, d_fake_c = self.multiD(x_fake, embedding)
            

            g_adv = self.hinge_loss(d_fake, target_is_real=True, for_discriminator=False)
            g_c_adv = self.hinge_loss(d_fake_c, target_is_real=True, for_discriminator=False)
            kl_loss = self.KL_loss(mu, logvar)
            
            gloss = 0.5 * (g_adv + g_c_adv) + 4.0 * kl_loss
            
            
            gloss.backward()
            self.g_optimizer.step()

            # smooth
            #update_average(self.Gt, self.G, beta=self.average_beta)


            # Logging.
            loss = {}
            loss['D/loss_real'] = d_real_adv.item()
            loss['D/loss_fake'] = d_fake_adv.item()
            loss['D/loss_real_c'] = d_real_c_adv.item()
            loss['D/loss_fake_c'] = d_fake_c_adv.item()
            
            loss['G/loss_fake'] = g_adv.item()
            loss['G/loss_fake_c'] = g_c_adv.item()
            loss['G/KL_loss'] = kl_loss.item()
            

            # Print out training information.
            if (i+1) % self.log_step == 0:
                et = time.time() - start_time
                et = str(datetime.timedelta(seconds=et))[:-7]
                log = "Elapsed [{}], Iteration [{}/{}]".format(et, i+1, self.num_iters)
                for tag, value in loss.items():
                    log += ", {}: {:.4f}".format(tag, value)
                print(log)

                #if self.use_tensorboard:
                #    for tag, value in loss.items():
                #        self.logger.scalar_summary(tag, value, i+1)

            # Translate fixed images for debugging.
            if (i+1) % self.sample_step == 0:
                self.G.eval()
                x_fake_list = [x]
                #z = zdist.sample((batch_size,))
                
                with torch.no_grad():
                    
                    
                    for _ in range(5):
                        z = self.G.getz(batch_size).to(self.device)
                        x_fake, _, _ = self.G(embedding, z)
                        x_fake_list.append(x_fake)
                    
                self.G.train()
                
                x_concat = torch.cat(x_fake_list, dim=3)
                sample_path = os.path.join(self.sample_dir, '{}-images.jpg'.format(i+1))
                save_image_q(self.denorm(x_concat.data.cpu()), sample_path, nrow=1, padding=0)
                    

            # Save model checkpoints.
            if (i+1) % self.model_save_step == 0:
                G_path = os.path.join(self.model_save_dir, '{}-G.ckpt'.format(i+1))
                #Gt_path = os.path.join(self.model_save_dir, '{}-Gt.ckpt'.format(i+1))
                D_path = os.path.join(self.model_save_dir, '{}-D.ckpt'.format(i+1))
                
                torch.save(self.G.state_dict(), G_path)
                #torch.save(self.Gt.state_dict(), Gt_path)
                torch.save(self.Ds.state_dict(), D_path)
                print('Saved model checkpoints into {}...'.format(self.model_save_dir))


            # Decay lr
            if (i+1) % self.lr_update_step == 0 and (i+1) > (self.num_iters - self.num_iters_decay):
                g_lr -= (self.g_lr / float(self.num_iters_decay))
                d_lr -= (self.d_lr / float(self.num_iters_decay))
                self.update_lr(g_lr, d_lr)
                print ('Decayed learning rates, g_lr: {}, d_lr: {}.'.format(g_lr, d_lr))

    def getRandomAngles(self, batch_size):
        return self.sample_angles(bs=batch_size,
                      min_angle_yaw=0,
                      max_angle_yaw=0,
                      min_angle_pitch=-np.pi/3.,
                      max_angle_pitch=np.pi/3.,
                      min_angle_roll=0,
                      max_angle_roll=0)

    def test(self, test_iters=None):
        """Translate images using StarGAN trained on a single dataset."""
        # Load the trained generator.
        if test_iters is not None:
            self.restore_model(test_iters)

        self.eval_model()
        
        #random.seed(0)

        #np.random.seed(0)

        #torch.manual_seed(0)
        torch.cuda.manual_seed_all(0)
            
        # Set data loader.
        data_loader = self.loader
        
        
        with torch.no_grad():
            for i in range(100):
                z = torch.randn(1, 128).to(self.device)
                mz = self.mapping(z)
                
                for j in range(20):
                
         
                    # Translate images.
                    x_fake_list = []

                    angles = self.get_angles(bs=1,
                          angle_yaw=0,
                          angle_pitch=(j/20.0)*np.pi/3.*2 - np.pi/3.,
                          angle_roll=0)
                    theta = self.get_theta(angles).to(self.device)


                    x_fake = self.G(mz, theta)

                    x_fake_list.append(x_fake)

                    # Save the translated images.
                    try:
                        x_concat = torch.cat(x_fake_list, dim=3)
                        result_path = os.path.join(self.result_dir, '{}-images.jpg'.format((20*i+j+1)))
                        save_image_q(self.denorm(x_concat.data.cpu()), result_path, nrow=1, padding=0)
                        print('Saved real and fake images into {}...'.format(result_path))
                    except:
                        pass

    def inter(self, test_iters=None):
        """Translate images using StarGAN trained on a single dataset."""
        # Load the trained generator.
        if test_iters is not None:
            self.restore_model(test_iters)

        self.eval_model()
        
        #random.seed(0)

        #np.random.seed(0)

        #torch.manual_seed(0)
        torch.cuda.manual_seed_all(0)
            
        # Set data loader.
        data_loader = self.loader

        zdist = self.get_zdist("gauss", self.z_dim, device=self.device)
        style_zdist = self.get_zdist("gauss", self.style_z_dim, device=self.device)
                
        noises_dist = [self.get_noise("gauss",(min(64 * 2**(int(np.log2(256 / 4))-i-1),512),4*2**i,4*2**i), device=self.device) for i in range(int(np.log2(256 / 4)))]
        
        z = zdist.sample((1,))
        style_z = self.mapping(style_zdist.sample((1,)))
        style_z_bac = self.mapping(style_zdist.sample((1,)))
        
        noises = [noises_dist[i].sample((1,)) for i in range(len(noises_dist))]
        noise_list = []
        style_list = []
        for j in range(100):
            torch.cuda.manual_seed_all(j)
            style_z = self.mapping(style_zdist.sample((1,)))
            style_list.append(style_z)
            noises = [noises_dist[i].sample((1,)) for i in range(len(noises_dist))]
            noise_list.append(noises)
        
            
        with torch.no_grad():
            inte = 5
            for i in range(100*inte):

                # Prepare input images and target domain labels.
                #x_real = x_real.to(self.device)
                
                # Translate images.
                x_fake_list = []
                
                t = 1-0.5*(np.cos(3.141592*float(i%inte)/inte)+1.)

                
                style_a = style_list[i//inte]
                style_b = style_list[i//inte+1]
                
                style_z = style_b*t+style_a*(1.0-t)
                
                
                noise_a = noise_list[i//inte]
                noise_b = noise_list[i//inte+1]
                noises = [noise_b[j]*t+noise_a[j]*(1.0-t) for j in range(len(noises_dist))]
                # print( noises[0][0,0])
                #z = zdist.sample((1,))
                
                #noises = [noises_dist[i].sample((1,)) for i in range(len(noises_dist))]
                #for j in range(6):
                    #noises[-1-j] = noises_dist[-1-j].sample((1,))
                #style_z = self.mapping(style_zdist.sample((1,)))
                #style_z2 = self.mapping(style_zdist.sample((1,)))     
                #style_z = torch.cat((style_z_bac[:-1],style_z2[-1:]),1)

                #if (i) % 10 == 0: style_z = self.mapping(style_zdist.sample((1,)))
                
                
                x_fake_list.append(self.Gt(z,  style_z, noises))

                # Save the translated images.
                try:
                    x_concat = torch.cat(x_fake_list, dim=3)
                    result_path = os.path.join("inter", '{}-images.jpg'.format(i+1))
                    save_image_q(self.denorm(x_concat.data.cpu()), result_path, nrow=1, padding=0)
                    print('Saved real and fake images into {}...'.format(result_path))
                except:
                    pass
                
                
    def rot_matrix_x(self, theta):
        """
        theta: measured in radians
        """
        mat = np.zeros((3,3)).astype(np.float32)
        mat[0, 0] = 1.
        mat[1, 1] = np.cos(theta)
        mat[1, 2] = -np.sin(theta)
        mat[2, 1] = np.sin(theta)
        mat[2, 2] = np.cos(theta)
        return mat

    def rot_matrix_y(self, theta):
        """
        theta: measured in radians
        """
        mat = np.zeros((3,3)).astype(np.float32)
        mat[0, 0] = np.cos(theta)
        mat[0, 2] = np.sin(theta)
        mat[1, 1] = 1.
        mat[2, 0] = -np.sin(theta)
        mat[2, 2] = np.cos(theta)
        return mat

    def rot_matrix_z(self, theta):
        """
        theta: measured in radians
        """
        mat = np.zeros((3,3)).astype(np.float32)
        mat[0, 0] = np.cos(theta)
        mat[0, 1] = -np.sin(theta)
        mat[1, 0] = np.sin(theta)
        mat[1, 1] = np.cos(theta)
        mat[2, 2] = 1.
        return mat

    def pad_rotmat(self, theta):
        """theta = (3x3) rotation matrix"""
        return np.hstack((theta, np.zeros((3,1))))

    def sample_angles(self,
                      bs,
                      min_angle_yaw,
                      max_angle_yaw,
                      min_angle_pitch,
                      max_angle_pitch,
                      min_angle_roll,
                      max_angle_roll):
        """Sample random yaw, pitch, and roll angles"""
        angles = []
        for i in range(bs):
            rnd_angles = [
                np.random.uniform(min_angle_yaw, max_angle_yaw),
                np.random.uniform(min_angle_pitch, max_angle_pitch),
                np.random.uniform(min_angle_roll, max_angle_roll),
            ]
            angles.append(rnd_angles)
        return np.asarray(angles)
    
    def get_angles(self,
                      bs,
                      angle_yaw,
                      angle_pitch,
                      angle_roll):
        """Sample random yaw, pitch, and roll angles"""
        angles = []
        for i in range(bs):
            rnd_angles = [
                angle_yaw,
                angle_pitch,
                angle_roll,
            ]
            angles.append(rnd_angles)
        return np.asarray(angles)


    def get_theta(self, angles):
        '''Construct a rotation matrix from angles.
        Notes
        -----
        You will notice in the code that I am:
          - passing `angles_y` into `rot_matrix_x`
          - passing `angles_z` into `rot_matrix_y``
          - and passing `angles_x` into `rot_matrix_z`.
        This is intentional!!! I was exploring the effect of these
        rotation matrices on a toy MNIST example, and it appears that:
          - `rot_matrix_y` appears to be controlling yaw (which I call 'z')
          - `rot_matrix_x` appears to be controlling pitch (which I call 'y')
          - `rot_matrix_z` appears to be controlling roll (which I call 'x')`.
        I have no idea if this is some weird thing going on with the STN
        module, or if I have incorrectly defined the rotation matrix
        functions here.
        '''
        bs = len(angles)
        theta = np.zeros((bs, 3, 4))

        angles_yaw = angles[:, 0]
        angles_pitch = angles[:, 1]
        angles_roll = angles[:, 2]
        for i in range(bs):
            theta[i] = self.pad_rotmat(
                np.dot(np.dot(self.rot_matrix_z(angles_roll[i]), self.rot_matrix_y(angles_pitch[i])),
                       self.rot_matrix_x(angles_yaw[i]))
            )

        return torch.from_numpy(theta).float()

# Utility functions
def toogle_grad(model, requires_grad):
    for p in model.parameters():
        p.requires_grad_(requires_grad)

def weights_init(init_type='gaussian'):
    def init_fun(m):
        classname = m.__class__.__name__
        if (classname.find('Conv') == 0 or classname.find('Linear') == 0) and hasattr(m, 'weight'):
            # print m.__class__.__name__
            if init_type == 'gaussian':
                init.normal_(m.weight.data, 0.0, 0.02)
            elif init_type == 'xavier':
                init.xavier_normal_(m.weight.data, gain=math.sqrt(2))
            elif init_type == 'kaiming':
                init.kaiming_normal_(m.weight.data, a=0, mode='fan_in')
            elif init_type == 'orthogonal':
                init.orthogonal_(m.weight.data)
            elif init_type == 'default':
                pass
            else:
                assert 0, "Unsupported initialization: {}".format(init_type)
            if hasattr(m, 'bias') and m.bias is not None:
                init.constant_(m.bias.data, 0.0)

    return init_fun

def compute_grad2(d_out, x_in):
    batch_size = x_in.size(0)
    grad_dout = autograd.grad(
        outputs=d_out.sum(), inputs=x_in,
        create_graph=True, retain_graph=True, only_inputs=True
    )[0]
    grad_dout2 = grad_dout.pow(2)
    assert(grad_dout2.size() == x_in.size())
    reg = grad_dout2.view(batch_size, -1).sum(1)
    return reg


def update_average(model_tgt, model_src, beta):
    toogle_grad(model_src, False)
    toogle_grad(model_tgt, False)

    param_dict_src = dict(model_src.named_parameters())

    for p_name, p_tgt in model_tgt.named_parameters():
        p_src = param_dict_src[p_name]
        assert(p_src is not p_tgt)
        
        p_tgt.copy_(beta*p_tgt + (1. - beta)*p_src)

def save_image_q(tensor, filename, nrow=8, padding=2,
               normalize=False, range=None, scale_each=False, pad_value=0, quality=100):
    """Save a given Tensor into an image file.

    Args:
        tensor (Tensor or list): Image to be saved. If given a mini-batch tensor,
            saves the tensor as a grid of images by calling ``make_grid``.
        **kwargs: Other arguments are documented in ``make_grid``.
    """
    from torchvision.utils import make_grid
    from PIL import Image
    grid = make_grid(tensor, nrow=nrow, padding=padding, pad_value=pad_value,
                     normalize=normalize, range=range, scale_each=scale_each)
    ndarr = grid.mul(255).clamp(0, 255).byte().permute(1, 2, 0).cpu().numpy()
    im = Image.fromarray(ndarr)
    im.save(filename, 'JPEG', quality=quality, optimize=True)
