# StackGAN adain

import torch, torchvision
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import torch.nn.utils.spectral_norm as spectral_norm

def actvn(x):
    return F.leaky_relu(x, 2e-1)

def linear(in_ch, out_ch, spec=True, bias=True):
    if spec:
        return spectral_norm(nn.Linear(in_ch, out_ch, bias=bias))
    else:
        return nn.Linear(in_ch, out_ch, bias=bias)

def conv3x3(in_ch, out_ch, spec=True, bias=True):
    if spec:
        return spectral_norm(nn.Conv2d(in_ch, out_ch, kernel_size=3, stride=1, padding=1, bias=bias))
    else:
        return nn.Conv2d(in_ch, out_ch, kernel_size=3, stride=1, padding=1, bias=bias)

def conv1x1(in_ch, out_ch, spec=True, bias=True):
    if spec:
        return spectral_norm(nn.Conv2d(in_ch, out_ch, kernel_size=1, stride=1, padding=0, bias=bias))
    else:
        return nn.Conv2d(in_ch, out_ch, kernel_size=1, stride=1, padding=0, bias=bias)
    
def conv4x4(in_ch, out_ch, spec=True, bias=True):
    if spec:
        return spectral_norm(nn.Conv2d(in_ch, out_ch, kernel_size=4, stride=2, padding=1, bias=bias))
    else:
        return nn.Conv2d(in_ch, out_ch, kernel_size=4, stride=2, padding=1, bias=bias)
    
    
class ConditionalAugmentation(nn.Module):
    def __init__(self):
        super().__init__()
        self.embedding_dim = 1024
        self.ch = 128
        self.fc = nn.Sequential(
            linear(self.embedding_dim, self.ch * 2, bias=True, spec=False),
            nn.LeakyReLU(0.2, inplace=False),
        )
        
    def encode(self, text_embedding):
        x = self.fc(text_embedding)
        mu = x[:, :self.ch]
        logvar = x[:, self.ch:]
        
        return mu, logvar
    
    def reparametrize(self, mu, logvar):
        if not self.training:
            return mu
        
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        
        return eps * std + mu
    
    def forward(self, text_embedding):
        mu, logvar = self.encode(text_embedding)
        code = self.reparametrize(mu, logvar)
        
        return code, mu, logvar
        

class AdaptiveInstanceNormalization(nn.Module):
    def __init__(self, ch, z_dim):
        super().__init__()
        self.ch = ch
        self.param_free_norm = nn.BatchNorm2d(ch, affine=False)
        self.mlp = linear(z_dim, ch*2, spec=False)
        
    def forward(self, x, z):
        batch = x.size(0)
        x_norm = self.param_free_norm(x)
        z = self.mlp(z)
        sigma = z[:,:self.ch].view(batch, self.ch, 1, 1)
        beta = z[:,self.ch:].view(batch, self.ch, 1, 1)
       
        return (1. + sigma) * x_norm + beta



class ResNetBlock(nn.Module):
    def __init__(self, ch):
        super(ResNetBlock, self).__init__()
        
        
        #norm = nn.BatchNorm2d
        
        self.main = nn.Sequential(
            conv3x3(ch, ch),
            nn.BatchNorm2d(in_ch),
            nn.LeakyReLU(0.2, inplace=False),
            conv3x3(ch, ch),
            nn.BatchNorm2d(out_ch),
        
        )
        
    def forward(self, x):
        return self.main(x) + x
        
class ResNetStyleBlock(nn.Module):
    def __init__(self, in_ch, out_ch, z_dim):
        super(ResNetStyleBlock, self).__init__()
        
        self.conv1 = conv3x3(in_ch, out_ch)
        self.conv2 = conv3x3(out_ch, out_ch)
        
        self.norm1 = AdaptiveInstanceNormalization(out_ch, z_dim)
        self.norm2 = AdaptiveInstanceNormalization(out_ch, z_dim)
        
        self.is_shortcut_cnn = (in_ch != out_ch)
        
        if self.is_shortcut_cnn:
            self.conv_s = conv1x1(in_ch, out_ch)
            self.norm_s = AdaptiveInstanceNormalization(out_ch, z_dim)
        
    def forward(self, x, z):
        h = x
        h = actvn(self.norm1(self.conv1(h), z))
        h = self.norm2(self.conv2(h), z)
        #h = self.conv1(actvn(self.norm1(h, z)))
        #h = self.conv2(actvn(self.norm2(h, z)))
        
        return self._shortcut(x, z) + h
    
    def _shortcut(self, x, z):
        if self.is_shortcut_cnn:
            return self.norm_s(self.conv_s(x), z)
        else:
            return x
        
        
class UPStyleBlock(nn.Module):
    def __init__(self, in_ch, out_ch, z_dim):
        super().__init__()
        self.up = nn.Upsample(scale_factor=2, mode='nearest')
        self.r = ResNetStyleBlock(in_ch, out_ch, z_dim)
        
    def forward(self, x, z):
        return self.r(self.up(x), z)


#def downBlock(in_ch, out_ch):
#    return nn.Sequential(
#        nn.LeakyReLU(0.2, inplace=False),
#        conv4x4(in_ch, out_ch),
#        nn.BatchNorm2d(out_ch),
#    )
    #return nn.Sequential(
    #    ResNetBlock(in_ch, out_ch),#conv4x4(in_ch, out_ch),
    #    nn.AvgPool2d(2, stride=2, padding=0)#nn.InstanceNorm2d(out_ch, affine=False),
    #    #ResNetBlock()#nn.LeakyReLU(0.2, inplace=False),
    #)
    
    
class Generator(nn.Module):
    def __init__(self, out_ch=3):
        super().__init__()
        ngf = 512
        #x_init = torch.randn((1, ngf*4*4))
        #x_init = (x_init - torch.mean(x_init)) / (torch.std(x_init) + 1e-8)
        #nn.init.orthogonal_(x_init.data)
        #self.x_init = nn.Parameter(x_init)
        #self.x_init.requires_grad = True
        
        self.ca = ConditionalAugmentation()
        
        #self.ed_mlp = nn.Sequential(
        #    linear(1024, 256, bias=True, spec=False),
        #    nn.LeakyReLU(0.2, inplace=False),
        #    linear(256, 256, bias=True, spec=False),
        #    nn.LeakyReLU(0.2, inplace=False),
        #    linear(256, 256, bias=True, spec=False),
        #    nn.LeakyReLU(0.2, inplace=False)
        #)
        
        self.fc = nn.Sequential(
                linear(128, ngf*4*4),
                nn.BatchNorm1d(ngf*4*4),
                nn.LeakyReLU(0.2, inplace=False)
                )
        ch_list = [
            ngf,       # 4x4
            ngf,       # 8x8
            ngf,       # 16x16
            ngf,  # 32x32
            ngf // 2,  # 64x64
            ngf // 4,  # 128x128
            ngf // 8, # 256x256
            ]
        self.conv0 = ResNetStyleBlock(ch_list[0], ch_list[0], z_dim=256) # 4x4
        self.conv1 = UPStyleBlock(ch_list[0], ch_list[1], z_dim=256) # 8x8
        self.conv2 = UPStyleBlock(ch_list[1], ch_list[2], z_dim=256) # 16x16
        self.conv3 = UPStyleBlock(ch_list[2], ch_list[3], z_dim=256) # 32x32
        self.conv4 = UPStyleBlock(ch_list[3], ch_list[4], z_dim=128) # 64x64
        self.conv5 = UPStyleBlock(ch_list[4], ch_list[5], z_dim=128) # 128x128
        self.conv6 = UPStyleBlock(ch_list[5], ch_list[6], z_dim=128) # 256x256
        self.final_conv = conv1x1(ch_list[6], out_ch)
                             
    def forward(self, embedding, z):
        bs = embedding.size(0)
        
        
        code, mu, logvar = self.ca(embedding)
        
        z_code = torch.cat((z, code), 1)
        
        h = z #torch.cat((z, code), 1)
        
        h = self.fc(h).view(bs, -1, 4, 4)
        h = self.conv0(h, z_code)
        h = self.conv1(h, z_code)
        h = self.conv2(h, z_code)
        h = self.conv3(h, z_code)
        h = self.conv4(h, code) # 64x64
        h = self.conv5(h, code)
        h = self.conv6(h, code)
        
        return F.tanh(self.final_conv(h)), mu, logvar
    
    def getz(self, batch_size):
        z = torch.randn(batch_size, 128)
        return z# / torch.sqrt(torch.mean(z**2, dim=1, keepdim=True) + 1e-8)
    
    
class Discriminator(nn.Module):
    def __init__(self, in_ch=3, input_size=256):
        super().__init__()
        ndf = 512
        self.ef = 128
        self.input_size = input_size
        
        ch_list = [
            ndf,       # 4x4
            ndf,       # 8x8
            ndf,       # 16x16
            ndf // 2,  # 32x32
            ndf // 4,  # 64x64
            ndf // 8,  # 128x128
            ndf // 16, # 256x256
            ]
        ch_list.reverse()
        print(ch_list)
        
        conv_layers = []
        conv_dim = 64
        
        self.first_conv = nn.Sequential(
            conv3x3(3, conv_dim, spec=False),
            nn.LeakyReLU(0.2, inplace=False),
        )
        
        conv_len = 4 if self.input_size == 64 else 5
        print(conv_len)
        for i in range(1, conv_len):
            conv_layers.append(conv4x4(conv_dim, conv_dim*2))
            conv_layers.append(nn.BatchNorm2d(conv_dim*2))
            conv_layers.append(nn.LeakyReLU(0.2, inplace=False))
            conv_dim = conv_dim * 2
            
        self.main = nn.Sequential(*conv_layers)
        
        #self.conv1 = downBlock(ch_list[0], ch_list[1]) # 128x128
        #self.conv2 = downBlock(ch_list[1], ch_list[2]) # 64x64
        #self.conv3 = downBlock(ch_list[2], ch_list[3]) # 32x32
        #self.conv4 = downBlock(ch_list[3], ch_list[4]) # 16x16
        #self.conv5 = downBlock(ch_list[4], ch_list[5]) # 8x8
        #self.conv6 = downBlock(ch_list[5], ch_list[6]) # 4x4

        

        self.embedding = nn.Sequential(
                nn.Linear(1024, self.ef),
                nn.LeakyReLU(0.2, inplace=False),
                )


        self.outlogits = nn.Sequential(
                #conv3x3(ndf, ndf),
                #nn.BatchNorm2d(ndf),
                #nn.LeakyReLU(0.2, inplace=False),
                nn.Conv2d(conv_dim, 1, kernel_size=4, stride=1, padding=0),
                )

        self.outlogits_c = nn.Sequential(
                conv3x3(conv_dim + self.ef, ndf, spec=True),
                nn.BatchNorm2d(ndf),
                nn.LeakyReLU(0.2, inplace=False),
                nn.Conv2d(ndf, 1, kernel_size=4, stride=1, padding=0),
                )
                
        
        #self.fc = linear(ndf*4*4, 1)
        #layers = []
        #layers+=[nn.Linear(4096*2, 4096), nn.LeakyReLU(0.2, inplace=False)]
        #layers+=[nn.Linear(4096, 128)]
        #self.d_hidden = nn.Sequential(*layers)
                             
    def forward(self, x, embedding):
        bs = x.size(0)
        h = self.first_conv(x)
        
        h = self.main(h)
        embedding = self.embedding(embedding)
        embedding = embedding.view(-1, self.ef, 1, 1).repeat(1, 1, h.size(2), h.size(3))

        h_c = torch.cat([h, embedding], 1)
        
        return self.outlogits(h).view(bs, -1), self.outlogits_c(h_c).view(bs, -1)#, self.d_hidden(h.view(bs, -1))#,self.outlogits_c(h_c).view(bs, -1)
    
        
        
                             
                             
                             
