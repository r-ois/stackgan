import os
import argparse
from solver import Solver
from data_loader import get_loader
from torch.backends import cudnn

def main():

    # training fast
    cudnn.benchmark = True

    # Directories.
    log_dir = "log"
    sample_dir = "sample"
    model_save_dir = "model"
    result_dir = "result"

    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    if not os.path.exists(model_save_dir):
        os.makedirs(model_save_dir)
    if not os.path.exists(sample_dir):
        os.makedirs(sample_dir)
    if not os.path.exists(result_dir):
        os.makedirs(result_dir)
    gpu = 3

    myDataset = get_loader('/usr/src/app/dataset/birds', image_size=256, crop_size=256,
               batch_size=8, mode="train", num_workers=4)

    solver = Solver(myDataset, gpu=gpu)
    solver.train()

if __name__ == "__main__":
    main()
