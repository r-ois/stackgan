# StackGAN BigGAN Base
かよちゃんに画像生成させるためのStackGANのコード

# システム図
<img src="https://gitlab.com/r-ois/stackgan/raw/master/kayo_system.png" width=50%>

## やることリスト
1. Text to GANベースの画像生成
  - StackGAN, HDGAN, MirrorGANなど
  - StackGANは実装しました
2. Style Transferを用いた画像のスタイル変換
  - Pix2PixHD, CycleGAN, MUNITなど
  - MUNITは実装しました（MUNITがこの中で一番精度が高い）
3. 画像からストローク検出
  - A Neural Representation of Sketch Drawingsなどを用いて行う（このあたりはもう少しサーベイします）
  - スケッチ画像からストロークまたは画像からストロークのどちらかが有効？

## 現状の課題
1. データセットの収集について
  - 対象をどこまで絞るかによって収集対象も変わる
  - アニメに特化...Pixiv, Twitterなどから収集
  - 著作権も考慮する必要あり(日本の法律上今は大丈夫)
2. 未知語に対する処理
  - その未知語を知らないと生成するのは不可能
  - Bingなどで画像を複数検索し特徴から画像生成する？